<?php
class Connection
{
	private static $_objetoAccesoDatos;
	private $_objetoPDO;

	private function __construct($archivo = "paginacion/libs/.conf/.conf.ini")
	{

		if (!$config = parse_ini_file($archivo,true)) throw new Exception("Error: conexion error!");
		$controller = $config['database'] ['driver'];
		$host = $config['database'] ['host'];
		$port = $config['database'] ['port'];
		$db_name = $config['database'] ['schema'];
		$user = $config['database'] ['username'];
		$pass = $config['database'] ['password'];
		$dns = $controller.":host" . $host . ";port = " . $port .";dbname=" . $db_name;

		try {

			$this->_objetoPDO = new PDO($dns, $user, $pass,array(PDO::ATTR_EMULATE_PREPARES => false,PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
			$this->_objetoPDO->exec("SET CHARACTER SET utf8");


		} catch (Exception $e) {
			print "Error!: " . $e->getMessage();
			die();

		}

	}

	public function prepararConsulta($sql)
	{
		return $this->_objetoPDO->prepare($sql);
	}
	public function retornarUltimoIdInsertado()
	{
		return $this->_objetoPDO->lastInsertId();
	}

	public static function singleton()
	{
		if (!self::$_objetoAccesoDatos instanceof self ) {
			self::$_objetoAccesoDatos = new self();
		}
		return self::$_objetoAccesoDatos;
	}

  public function ejecutarQuery($consulta)
  {
    return $consulta->execute();
  }




	// Evita que el objeto se pueda clonar
	public function __clone()
	{
		trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
	}
}
?>
